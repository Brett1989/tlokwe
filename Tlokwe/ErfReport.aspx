﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ErfReport.aspx.vb" Inherits="Tlokwe.ErfReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
      <style>
        td{
            padding-left: 5px;
        }
        th {
            color: black;
            background-color: gray;
            width: 200px;
            text-align: right;
        } 
        table, tr, th, td{
            border: 1px solid black;
            border-collapse: collapse;
             font-size: medium ;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="jumbotron">
          <h2 style="text-align:center;">REPORT FOR ERF: <%=Session("erfNo")%></h2>
          <h2 style="text-align:center;">in the Tolokwe City Council</h2>
          <h2 style="text-align:center;">Erf information: <%=Session("erfInfo") %></h2>
                <table  style="width:100%">
                    <tr>
                        <th colspan="1">Owner:</th>
                        <td colspan="3"><%=Session("owner") %></td>
                    </tr>
                    <tr>
                        <th colspan="1">Address:</th>
                        <td colspan="3"><%=Session("address") %></td>
                    </tr>
                    <tr>
                        <th>Zone:</th>
                        <td><%=Session("zone") %></td>
                        <th>Wyk:</th>
                        <td><%=Session("wyk") %></td>
                    </tr>
                     <tr>
                        <th>Size:</th>
                        <td><%=Session("size") %></td>
                        <th>Grond:</th>
                        <td><%=Session("grond") %></td>
                    </tr>
                     <tr>
                        <th colspan="1">Omskryf:</th>
                        <td colspan="3"><%=Session("omskryf") %></td>
                    </tr>                  
                  </table>
            <div style="text-align:center;">
                <iframe id="mapFrame"  src="Tlokwe_PotchGIS/index.html" runat="server" height="400" width="800" ></iframe>
            </div> 
        </div> 
</asp:Content>
