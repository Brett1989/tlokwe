﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="GisMeetings.aspx.vb" Inherits="Tlokwe.GisMeetings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2>GIS Meetings</h2>
        <ul>
            <li><a href="Docs/Meeting01- GisPlanning.ppt">GIS Steering Committee meeting on 6 February 2008</a></li>
            <li><a href="Docs/Meeting02 - ExtendedGisProposal.ppt">GIS Steering Committee meeting on 27 February 2008</a></li>
        </ul>
    </div>
</asp:Content>
