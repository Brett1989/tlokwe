﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class ErfReport
    Inherits System.Web.UI.Page

    Dim myConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Potch_sde").ConnectionString)
    Private Property cmd As SqlCommand
    Dim rdr As SqlDataReader


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim query As String
        Dim lat As String, lng As String
        myConn.Open()
        query = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where a.Erfverwysi = '" & Session("erfInfo") & "'"
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                lat = rdr.Item("LATITUDE")
                lng = rdr.Item("LONGITUDE")
                mapFrame.Src = "Tlokwe_PotchGIS/index.html?center=" & lat & "," & lng & "&scale=50000"
            Loop
        End If
        myConn.Close()
    End Sub

End Class