﻿Imports System.IO

Public Class Hotlink
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mapType As String
        Dim oid As String, refno As String, hyper As String

        mapType = Request.QueryString("map")

        Select Case mapType
            Case "base"
                refno = Request.QueryString("refno")
                For Each dir As String In System.IO.Directory.GetDirectories(MapPath("~/Geotechnical"))
                    Dim dirInfo As New System.IO.DirectoryInfo(dir)
                    If dirInfo.Name = refno Then
                        Dim paths As String() = New String() {("~/Geotechnical/" & refno)}
                        'This code sets RadFileExplorer's paths
                        RadFileExplorer1.Configuration.ViewPaths = paths
                        RadFileExplorer1.Configuration.UploadPaths = paths
                        RadFileExplorer1.Configuration.DeletePaths = paths

                        'Sets Max file size
                        RadFileExplorer1.Configuration.MaxUploadFileSize = 10485760

                        ' Enables Paging on the Grid
                        RadFileExplorer1.AllowPaging = True
                        ' Sets the page size
                        RadFileExplorer1.PageSize = 20
                    End If
                Next
            Case "dolomite"
                Dim temp As String
                Dim pos As Integer
                oid = Request.QueryString("oid")
                For Each file As String In System.IO.Directory.GetFiles(MapPath("~/Building Plan Applications Dolomite"))
                    Dim fileInfo As New System.IO.FileInfo(file)
                    temp = Path.GetFileNameWithoutExtension(fileInfo.Name)
                    oid = Replace(oid, ".tif", "")
                    oid = Replace(oid, "%20", " ")
                    oid = Replace(oid, "/", "_")
                    If temp = oid Then
                        Response.Redirect("Handler.ashx?path=" & "~/Building Plan Applications Dolomite/" & fileInfo.Name)
                    End If
                Next
            Case "housing"
                Dim temp As String
                Dim pos As Integer
                oid = Request.QueryString("oid")
                ''If InStrRev(oid, "/") > 0 Then
                ''    pos = InStrRev(oid, "/")
                ''   oid = Right(oid, pos - 1)
                ''    oid = Replace(oid, ".jpg", "")
                ''End If

                For Each file As String In System.IO.Directory.GetFiles(MapPath("~/Housing_Ward_Survey"))
                    Dim fileInfo As New System.IO.FileInfo(file)
                    temp = Path.GetFileNameWithoutExtension(fileInfo.Name)
                    If temp = oid Then
                        Response.Redirect("Handler.ashx?path=" & "~/Housing_Ward_Survey/" & fileInfo.Name)
                    End If
                Next
            Case "lums"
                Dim temp As String
                Dim pos As Integer
                hyper = Request.QueryString("hyperkey")
                hyper = Replace(hyper, "%20", " ")
                'pos = InStrRev(hyper, "/")
                'hyper = Right(hyper, pos - 3)
                '  get 1st foldera
                For Each file As String In System.IO.Directory.GetFiles(MapPath("~/Lums Scans/Business Rights - Province"))
                    Dim fileInfo As New System.IO.FileInfo(file)
                    temp = Path.GetFileNameWithoutExtension(fileInfo.Name)
                    If temp = hyper Then
                        Response.Redirect("Handler.ashx?path=" & "~/Lums Scans/Business Rights - Province/" & fileInfo.Name)
                    End If
                Next
                'get 2nd folder file
                For Each file As String In System.IO.Directory.GetFiles(MapPath("~/Lums Scans/Resorts - Province"))
                    Dim fileInfo As New System.IO.FileInfo(file)
                    temp = Path.GetFileNameWithoutExtension(fileInfo.Name)
                    If temp = hyper Then
                        Response.Redirect("Handler.ashx?path=" & "~/Lums Scans/Resorts - Province/" & fileInfo.Name)
                    End If
                Next
        End Select
    End Sub

End Class