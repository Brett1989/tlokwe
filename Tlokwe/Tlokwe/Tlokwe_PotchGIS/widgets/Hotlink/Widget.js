define(['dojo/_base/declare', 'jimu/BaseWidget','esri/tasks/IdentifyTask',
  'esri/tasks/IdentifyParameters',],
function(declare, BaseWidget, IdentifyTask, IdentifyParameters) {
  //To create a widget, you need to derive from BaseWidget.
  return declare([BaseWidget], {

    // Custom widget code goes here

    baseClass: 'hotlink',
    // this property is set by the framework when widget is loaded.
    // name: 'Hotlink',
    // add additional properties here

    //methods to communication with app container:
    postCreate: function() {
      this.inherited(arguments);
      console.log('Hotlink::postCreate');
	  mainMap = this.map;
	  console.log(mainMap);
	  //user clicked point
		mainMap.on("click", function (evt) {
		
			//get site name clicked with id
			 //initialize identity task
			identifyTask = new IdentifyTask("http://41.161.83.226:6080/arcgis/rest/services/Tlokwe/PotchGIS/MapServer");
		   //console.log(mainMap);
			identifyParams = new IdentifyParameters();
            identifyParams.tolerance = 3;
            identifyParams.returnGeometry = true;
            identifyParams.layerIds = [28];
            identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_VISIBLE;
            identifyParams.width = mainMap.width;
            identifyParams.height = mainMap.height;
			
            identifyParams.geometry = evt.mapPoint;
			identifyParams.mapExtent = mainMap.extent;
			identifyTask.execute(identifyParams, function (idResults) {
			    console.log(idResults[0].feature.attributes.OBJECTID);
			    var refno = idResults[0].feature.attributes.OBJECTID;
			    document.cookie = "map=base";
			    window.open('../Hotlink.aspx?map=housing&refno=' + refno, '_target');
            });
		  
		   //console.log(evt.graphic.attributes.OID);
		  // window.open('../Hotlink.aspx', '_blank');
		});
    }

    // startup: function() {
	 
     //}
	 
    // onOpen: function(){
    //   console.log('Hotlink::onOpen');
    // },

    // onClose: function(){
    //   console.log('Hotlink::onClose');
    // },

    // onMinimize: function(){
    //   console.log('Hotlink::onMinimize');
    // },

    // onMaximize: function(){
    //   console.log('Hotlink::onMaximize');
    // },

    // onSignIn: function(credential){
    //   console.log('Hotlink::onSignIn', credential);
    // },

    // onSignOut: function(){
    //   console.log('Hotlink::onSignOut');
    // }

    // onPositionChange: function(){
    //   console.log('Hotlink::onPositionChange');
    // },

    // resize: function(){
    //   console.log('Hotlink::resize');
    // }

//methods to communication between widgets:

  });

});
