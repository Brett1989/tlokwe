﻿Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Text
Imports Telerik.Web.UI

Public Class Handler
    Implements System.Web.IHttpHandler

#Region "IHttpHandler Members"

    Private _context As HttpContext
    Private Property Context() As HttpContext
        Get
            Return _context
        End Get
        Set(value As HttpContext)
            _context = value
        End Set
    End Property


    Public Sub ProcessRequest(context__1 As HttpContext) Implements IHttpHandler.ProcessRequest
        Context = context__1
        Dim filePath As String = context__1.Request.QueryString("path")
        filePath = context__1.Server.MapPath(filePath)

        If filePath Is Nothing Then
            Return
        End If

        Dim streamReader As New System.IO.StreamReader(filePath)
        Dim br As New System.IO.BinaryReader(streamReader.BaseStream)

        Dim bytes As Byte() = New Byte(streamReader.BaseStream.Length - 1) {}

        br.Read(bytes, 0, CInt(streamReader.BaseStream.Length))

        If bytes Is Nothing Then
            Return
        End If

        streamReader.Close()
        br.Close()
        Dim extension As String = System.IO.Path.GetExtension(filePath)
        Dim fileName As String = System.IO.Path.GetFileName(filePath)
        WriteFile(bytes, fileName, extension, context__1.Response)
        'If extension = ".jpg" Then
        '    ' Handle *.jpg and

        'ElseIf extension = ".gif" Then
        '    ' Handle *.gif
        '    WriteFile(bytes, fileName, "image/gif gif", context__1.Response)
        'ElseIf extension = ".xml" Then
        '    ' Handle *.gif
        '    WriteFile(bytes, fileName, "text/xml", context__1.Response)
        'End If

    End Sub

    ''' <summary>
    ''' Sends a byte array to the client
    ''' </summary>
    ''' <param name="content">binary file content</param>
    ''' <param name="fileName">the filename to be sent to the client</param>
    ''' <param name="contentType">the file content type</param>
    Private Sub WriteFile(content As Byte(), fileName As String, contentType As String, response As HttpResponse)
        response.Buffer = True
        response.Clear()
        response.ContentType = contentType

        response.AddHeader("content-disposition", Convert.ToString("attachment; filename=") & fileName)

        response.BinaryWrite(content)
        response.Flush()
        response.[End]()
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

#End Region

End Class