﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Search.aspx.vb" Inherits="Tlokwe.Search" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron vertical-center">
        <h2 style="text-align:center;">Search for Property information using the GIS platform.</h2>
        <hr />
        <div style="text-align:center;" id="erf" runat="server">
            <h4 style="text-align:center;">Erf Search Tool</h4>
            <h5 style="color:green">Search Tips:</h5>
            <h6>You can use wildcards to help your search:</h6>
            <h6>To return all erf numbers containing 123, type *123*</h6>
            <h6>To return all erf numbers ending in 123, type *123</h6>
            <h6>To return all erf numbers starting with 123, type 123*</h6>         
                <asp:Label style="line-height:25px;" Height="25" runat="server"  Text="Erf Number:" Font-Bold="true"></asp:Label>
                <asp:TextBox ID="txtErf" style="line-height:30px;" height="30" runat="server" ></asp:TextBox>
                <telerik:RadButton style="line-height:30px;" Height="30" ID="btnErfSearch" runat="server" Text="Search" Skin="Silk" ></telerik:RadButton>
            
            <telerik:RadGrid AllowPaging="true" PageSize="10" ClientSettings-EnableRowHoverStyle="true" Visible="false" ID="searchGrid" runat="server" CellSpacing="-1" DataSourceID="erfSQL" GridLines="Both" GroupPanelPosition="Top" Skin="Silk">
                <MasterTableView AutoGenerateColumns="False" DataSourceID="erfSQL">
                    <Columns>
                        <telerik:GridBoundColumn DataField="PARCEL_LAB" DataType="System.Decimal" FilterControlAltText="Filter Stand_No column" HeaderText="Stand No" SortExpression="Stand_No" UniqueName="Stand_No">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Straatadre" DataType="System.Decimal" FilterControlAltText="Filter Ward column" HeaderText="Address" SortExpression="Address" UniqueName="Address">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Soneringsk" FilterControlAltText="Filter Zoning column" HeaderText="Zoning" SortExpression="Zoning" UniqueName="Zoning">
                        </telerik:GridBoundColumn>
                       
                        <telerik:GridButtonColumn HeaderText="View Map" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Map" CommandName="Map" ButtonType="PushButton" Text="Map"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn HeaderText="Report" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Report" CommandName="Report" ButtonType="PushButton" Text="Report" ></telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource ID="erfSQL" runat="server" ConnectionString="<%$ ConnectionStrings:Potch_sde %>" SelectCommand="">            
            </asp:SqlDataSource>
        </div>         
        <div style="text-align:center;" id="street" runat="server">
            <h4 style="text-align:center;">Address Search Tool</h4>
            <h5 style="color:green">Search Tips:</h5>
            <h6>You can use wildcards to help your search:</h6>
            <h6>To return all street names starting with NELSON, type NELSON*</h6>  
            <h6>To return a street name and number: Type NELSON*31 to return NELSON MANDELA DRIVE 31</h6>
                   
                <asp:Label style="line-height:25px;" Height="25" runat="server"  Text="Type a Street Name or Number:" Font-Bold="true"></asp:Label>
                <asp:TextBox ID="txtAddress" style="line-height:30px;" height="30" runat="server" ></asp:TextBox>
                <telerik:RadButton style="line-height:30px;" Height="30" ID="btnAddress" runat="server" Text="Search Address" Skin="Silk" ></telerik:RadButton>

             <telerik:RadGrid AllowPaging="true" PageSize="10" ClientSettings-EnableRowHoverStyle="true" Visible="false" ID="addressGrid" runat="server" CellSpacing="-1" DataSourceID="erfSQL" GridLines="Both" GroupPanelPosition="Top" Skin="Silk">
                <MasterTableView AutoGenerateColumns="False" DataSourceID="addressSQL">
                    <Columns>
                        <telerik:GridBoundColumn DataField="PARCEL_LAB" DataType="System.Decimal" FilterControlAltText="Filter Stand_No column" HeaderText="Stand No" SortExpression="Stand_No" UniqueName="Stand_No">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Straatadre" DataType="System.Decimal" FilterControlAltText="Filter Ward column" HeaderText="Address" SortExpression="Address" UniqueName="Address">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Soneringsk" FilterControlAltText="Filter Zoning column" HeaderText="Zoning" SortExpression="Zoning" UniqueName="Zoning">
                        </telerik:GridBoundColumn>
                       
                        <telerik:GridButtonColumn HeaderText="View Map" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Map" CommandName="Map" ButtonType="PushButton" Text="Map"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn HeaderText="Report" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Report" CommandName="Report" ButtonType="PushButton" Text="Report" ></telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource ID="addressSQL" runat="server" ConnectionString="<%$ ConnectionStrings:Potch_sde %>" SelectCommand="">            
            </asp:SqlDataSource>
        </div>
        <div style="text-align:center;" id="count" runat="server">
             <h4 style="text-align:center;">Count Erven Tool</h4>
            <br />
            <asp:Label runat="server" Text="Choose a Land Boundary:" Font-Bold="true"></asp:Label>
            <telerik:RadDropDownList ID="dropErvenCount" DefaultMessage="Please Select a Land Boundary" runat="server" DataSourceID="Potch_sde" DataTextField="BOUNDARY_L" DataValueField="BOUNDARY_L" Skin="Silk" Width="250px"></telerik:RadDropDownList>            
            <telerik:RadButton Height="30" ID="btnErvenCount" runat="server" Text="Count Erven" Skin="Silk"></telerik:RadButton>
            <h5 visible="false"  id="erverHeader" runat="server" style="background-color:lightblue; color:white"><%= Session("ervenCount") %> erven Found in <%= Session("ervenSelected") %></h5>
            <asp:Button Visible="false" ID="btnExport" runat="server" Text="Export To Excel"/> &nbsp
            <asp:Button Visible="false" ID="btnZoomTo" runat="server" Text="Zoom To Results" />
        </div>
        <div style="text-align:center;" id="age" runat="server">
             <h4 style="text-align:center;">Search by Arrears </h4>

            <asp:Label runat="server" Text="Select Ward: " Font-Bold="true"></asp:Label>&nbsp
             <telerik:RadDropDownList ID="dropWardList" DefaultMessage="Please Select a Ward" runat="server" DataSourceID="Potch_sde2" DataTextField="WARD_NO" DataValueField="WARD_NO" Skin="Silk" Width="160px"></telerik:RadDropDownList><br />
            <asp:Label runat="server" Text="Select Age Analysis Peroid:" Font-Bold="true"></asp:Label>
             <telerik:RadDropDownList ID="dropAgePeroid" runat="server" DefaultMessage="Select Age Peroid" Skin="Silk">
                 <Items>
                     <telerik:DropDownListItem runat="server" Text="Current" />
                     <telerik:DropDownListItem runat="server" Text="30 Days" />
                     <telerik:DropDownListItem runat="server" Text="60 Days" />
                     <telerik:DropDownListItem runat="server" Text="90 Days" />
                     <telerik:DropDownListItem runat="server" Text="120 Days" />
                 </Items>                                 
             </telerik:RadDropDownList> <br />
            <telerik:RadButton Height="30" ID="btnAgeSearch" runat="server" Text="Search" Skin="Silk"></telerik:RadButton>
             <h5 visible="false"  id="ageHeader" runat="server" style="background-color:lightblue; color:white"><%= Session("ageCount") %> erven Found in Ward <%= Session("wardSelected") %> with <%= Session("ageSelected") %> arrears</h5>
            <asp:Button Visible="false" ID="btnAgeExport" runat="server" Text="Export To Excel" /> &nbsp
            <asp:Button Visible="false" ID="btnAgeZoomto" runat="server" Text="Zoom To Results" />

             <telerik:RadGrid AllowPaging="true" PageSize="500" ClientSettings-EnableRowHoverStyle="true" Visible="false" ID="arrearsGrid" runat="server" CellSpacing="-1" DataSourceID="arrearsSQL" GridLines="Both" GroupPanelPosition="Top" Skin="Silk">
                <MasterTableView AutoGenerateColumns="False" DataSourceID="arrearsSQL">
                    <Columns>
                        <telerik:GridBoundColumn DataField="Eienaarnaa" DataType="System.Single" FilterControlAltText="Filter Eienaarnaa column" HeaderText="Owner" SortExpression="Owner" UniqueName="Owner">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Straatadre" DataType="System.Single" FilterControlAltText="Filter Ward column" HeaderText="Address" SortExpression="Address" UniqueName="Address">
                        </telerik:GridBoundColumn>   
                  
                        <telerik:GridButtonColumn HeaderText="View Map" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Map" CommandName="Map" ButtonType="PushButton" Text="Map"></telerik:GridButtonColumn>
                        <telerik:GridButtonColumn HeaderText="Report" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" UniqueName="Report" CommandName="Report" ButtonType="PushButton" Text="Report" ></telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource ID="arrearsSQL" runat="server" ConnectionString="<%$ ConnectionStrings:Potch_sde %>" SelectCommand="">            
            </asp:SqlDataSource>
        </div>
        <hr />
    </div>
    
    <asp:SqlDataSource ID="Potch_sde" runat="server" ConnectionString="<%$ ConnectionStrings:Potch_sde %>" SelectCommand="SELECT DISTINCT [BOUNDARY_L] FROM [LANDPARCEL] ORDER BY [BOUNDARY_L]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Potch_sde2" runat="server" ConnectionString="<%$ ConnectionStrings:Potch_sde %>" SelectCommand="SELECT DISTINCT [WARD_NO] FROM potch_sde.sde.[WARDS]"></asp:SqlDataSource>
   
</asp:Content>
