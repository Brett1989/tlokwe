﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Hotlink.aspx.vb" Inherits="Tlokwe.Hotlink" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
    <script>
        function getDownloadCheckbox() {
            return $get("<%= chkbxDownoaldFile.ClientID %>");
        }

        (function (global, undefined) {
            function OnClientFileOpen(oExplorer, args) {
                var item = args.get_item();
                var fileExtension = item.get_extension();

                var fileDownloadMode = global.getDownloadCheckbox().checked;
                if ((fileDownloadMode == false)) {// Download the file 
                    // File is a image document, do not open a new window
                    args.set_cancel(true);

                    // Tell browser to open file directly
                    var requestImage = "Handler.ashx?path=" + item.get_url();
                    document.location = requestImage;
                }
            }

            global.OnClientFileOpen = OnClientFileOpen;
        })(window);
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
    <h2 style="text-align:center;">Hotlink Tool</h2>
        <hr />
        <asp:CheckBox ID="chkbxDownoaldFile" runat="server" Checked="true" Text="View Only (Uncheck to Download the File)" AutoPostBack="false" />
        <br />
       <div class="row">
                <div class="col-md-12" style="text-align: center">
                    <telerik:RadFileExplorer OnClientFileOpen="OnClientFileOpen" runat="server" ID="RadFileExplorer1" Width="1000" Height="0px" Skin="Vista">
                    
                    </telerik:RadFileExplorer>
                 </div>
         </div>
    </div>
</asp:Content>
