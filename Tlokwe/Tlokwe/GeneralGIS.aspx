﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="GeneralGIS.aspx.vb" Inherits="Tlokwe.GeneralGIS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2>General GIS Information</h2>
        <div class="row">
            <div class="col-md-6"  style="text-align:center">
                <p><img src="images/esri.PNG" align="top" /> <a href="http://www.esri.com">www.esri.com</a></p>
            </div>
            <div class="col-md-6" style="text-align:center">
                <p><img src="images/giscoe.PNG" align="top"  /> <a href="http://www.giscoe.com">www.giscoe.com</a></p>
            </div>
        </div>
        
    </div>
</asp:Content>
