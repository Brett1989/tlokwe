﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="Tlokwe._Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron vertical-center">
        <h3 style="text-align:center;margin-top:-20px;">Welcome to the Tlokwe Intranet.</h3>

       <div class="row" >
           <div class="col-md-6" style="text-align:right;">
               <img src="images/Pic_1.jpg" />
           </div>
           <div class="col-md-6" style="text-align:left;">
               <img src="images/Pic_2.jpg" />
           </div>
       </div>
        <br />
        <div class="row">
           <div class="col-md-6"  style="text-align:right;">
               <img src="images/Pic_3.jpg" />
           </div>
           <div class="col-md-6" style="text-align:left;">
               <img src="images/Pic_4.jpg" />
           </div>
       </div>
        <div class="row">
            <div class="col=-md-12" style="text-align:center ;">
                <img src="images/finance-map.PNG" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-3" style="text-align:center;">
                <a href="Basemap.aspx">Link to NEW e_GIS Map</a>
            </div>
            <div class="col-md-3" style="text-align:center;" id="dolMap">
                 <a href="Dolomitemap.aspx">Link to Dolomite Web App</a>
            </div>
            <div class="col-md-3" style="text-align:center;">
                 <a href="Lumsmap.aspx">Link to LUMS Web App</a>
            </div>
            <div class="col-md-3" style="text-align:center;">
                 <a href="Housingmap.aspx">Link to Housing Web App</a>
            </div>
        </div>
    </div>
    <telerik:RadWindow ID="RadWindow1" runat="server" Skin="Silk" Modal="true" OpenerElementID="dolMap" Width="400">
        <ContentTemplate>
            <h4>Please Enter Username and Password.</h4>
                <asp:Label Width="50" runat="server" Text="Username: "></asp:Label> &nbsp&nbsp&nbsp
                <input type="text" ID="uname" ></input> <br /><br />
                <asp:Label Width="50" runat="server" Text="Password: "></asp:Label> &nbsp&nbsp&nbsp
                <input type="password" ID="pass" ></input> <br /><br />
                <input style="Width:70px" type="button" id="demo" value="Submit"/>
                <label id="error" style="color:red"></label>
            </div>
            <script>
                document.getElementById("demo").addEventListener("click", validate);

                 function validate() {
                     var uname = document.getElementById("uname").value;
                     var pass = document.getElementById("pass").value;
                     if (uname == "calypso" && pass == "test")
                     {
                         window.location = "Dolomitemap.aspx";
                     }
                     else {
                         document.getElementById("error").innerHTML = "Username/Password Incorrect!"
                     }
                }
            </script>
        </ContentTemplate>
    </telerik:RadWindow>

</asp:Content>
