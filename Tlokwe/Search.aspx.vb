﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports Telerik.Web.UI
Imports System.IO
Imports System.Drawing
Imports ClosedXML

Public Class Search
    Inherits System.Web.UI.Page

    Dim myConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Potch_sde").ConnectionString)
    Private Property cmd As SqlCommand
    Dim rdr As SqlDataReader


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim page As String

        erf.Visible = False
        street.Visible = False
        count.Visible = False
        age.Visible = False

        page = Request.QueryString("page")
        Select Case page
            Case "erfNumber"
                erf.Visible = True
            Case "streetAddress"
                street.Visible = True
            Case "erven"
                count.Visible = True
            Case "Arrears"
                age.Visible = True
        End Select

    End Sub

    Private Sub btnErfSearch_Click(sender As Object, e As EventArgs) Handles btnErfSearch.Click
        erfSearch(txtErf.Text)
    End Sub

    Private Sub searchGrid_PageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles searchGrid.PageIndexChanged
        erfSearch(txtErf.Text)
    End Sub

    Private Sub searchGrid_ItemCommand(sender As Object, e As Global.Telerik.Web.UI.GridCommandEventArgs) Handles searchGrid.ItemCommand
        If e.CommandName = "Report" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("erfNo") = item("Stand_No").Text
            generateReport(e.CommandName, Session("erfNo"), "erf")
        ElseIf e.CommandName = "Map" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("straatAdre") = item("Address").Text
            Response.Redirect("Basemap.aspx?zoom=yes")
        End If
    End Sub

    Private Sub addressGrid_ItemCommand(sender As Object, e As Global.Telerik.Web.UI.GridCommandEventArgs) Handles addressGrid.ItemCommand
        If e.CommandName = "Report" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("erfNo") = item("Stand_No").Text
            generateReport(e.CommandName, Session("erfNo"), "address")
        ElseIf e.CommandName = "Map" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("straatAdre") = item("Address").Text
            Response.Redirect("Basemap.aspx?zoom=yes")
        End If
    End Sub

    Private Sub btnAddress_Click(sender As Object, e As EventArgs) Handles btnAddress.Click
        addressSearch(txtAddress.Text)
    End Sub

    Private Sub btnErvenCount_Click(sender As Object, e As EventArgs) Handles btnErvenCount.Click
        Dim query As String
        Dim count As Integer

        erverHeader.Visible = True
        btnExport.Visible = True
        btnZoomTo.Visible = True
        count = 0
        query = "select * FROM [potch_sde].[sde].[LANDPARCEL] where BOUNDARY_L = '" & dropErvenCount.SelectedText & "'"
        myConn.Open()
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                count = count + 1
            Loop
        End If
        Session("ervenCount") = count
        Session("ervenSelected") = dropErvenCount.SelectedValue
        myConn.Close()

    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        ExportToExcel("select * FROM [potch_sde].[sde].[LANDPARCEL] where BOUNDARY_L = '" & dropErvenCount.SelectedText & "'", Session("ervenSelected"))
    End Sub

    Sub erfSearch(erf As String)
        Dim pos As Integer
        Dim both, lft, rght As Boolean


        erf = txtErf.Text
        both = False
        lft = False
        rght = False
        pos = InStr(erf, "*")
        If pos < 2 And pos > 0 Then
            erf = Right(erf, Len(erf) - pos)
            If InStr(erf, "*") > 0 Then
                pos = InStr(erf, "*")
                erf = Left(erf, pos - 1)
                both = True
            Else
                lft = True
            End If
        ElseIf pos > 2 Then
            erf = Left(erf, pos - 1)
            rght = True
        ElseIf pos = 0 Then
            erf = txtErf.Text
        End If

        If lft Then
            erfSQL.SelectCommand = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where B.PARCEL_LAB LIKE '%" & erf & "'"
        ElseIf pos = 0 Then
            erfSQL.SelectCommand = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where B.PARCEL_LAB = '" & erf & "'"
        ElseIf both Then
            erfSQL.SelectCommand = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where B.PARCEL_LAB LIKE '%" & erf & "%'"
        ElseIf rght Then
            erfSQL.SelectCommand = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where B.PARCEL_LAB LIKE '" & erf & "%'"
        End If
        searchGrid.Visible = True
        'Session("Stand_No") = txtErf.Text
    End Sub

    Sub addressSearch(erf As String)
        Dim pos As Integer
        Dim both, lft, rght As Boolean


        erf = txtAddress.Text
        both = False
        lft = False
        rght = False
        pos = InStr(erf, "*")
        If pos < 2 And pos > 0 Then
            erf = Right(erf, Len(erf) - pos)
            If InStr(erf, "*") > 0 Then
                pos = InStr(erf, "*")
                erf = Left(erf, pos - 1)
                both = True
            Else
                lft = True
            End If
        ElseIf pos > 2 Then
            erf = Left(erf, pos - 1)
            rght = True
        ElseIf pos = 0 Then
            erf = txtErf.Text
        End If

        If lft Then
            addressSQL.SelectCommand = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where a.Straatadre LIKE '%" & erf & "'"
        ElseIf pos = 0 Then
            addressSQL.SelectCommand = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where a.Straatadre  = '" & erf & "'"
        ElseIf both Then
            addressSQL.SelectCommand = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where a.Straatadre  LIKE '%" & erf & "%'"
        ElseIf rght Then
            addressSQL.SelectCommand = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where a.Straatadre  LIKE '" & erf & "%'"
        End If
        addressGrid.Visible = True
        'Session("Stand_No") = txtErf.Text
    End Sub

    Protected Sub btnAgeSearch_Click(sender As Object, e As EventArgs) Handles btnAgeSearch.Click
        Dim query As String
        Dim count As Integer

        ageHeader.Visible = True
        btnAgeExport.Visible = True
        btnAgeZoomto.Visible = True
        count = 0
        query = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where b.Ward = " & dropWardList.SelectedValue
        myConn.Open()
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                count = count + 1
            Loop
        End If
        Session("ageCount") = count
        Session("wardSelected") = dropWardList.SelectedValue
        Session("ageSelected") = dropAgePeroid.SelectedText
        myConn.Close()
        arrearsGrid.Visible = True
        arrearsSQL.SelectCommand = query
    End Sub

    Protected Sub btnAgeExport_Click(sender As Object, e As EventArgs) Handles btnAgeExport.Click
        ExportToExcel("select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where b.Ward = " & dropWardList.SelectedValue, Session("wardSelected"))
    End Sub

    Private Sub arrearsGrid_ItemCommand(sender As Object, e As Global.Telerik.Web.UI.GridCommandEventArgs) Handles arrearsGrid.ItemCommand
        If e.CommandName = "Report" Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Session("owner") = item("Owner").Text
            Session("arrearsAddress") = item("Address").Text
            generateReport(e.CommandName, Session("owner"), "arrears")
        ElseIf e.CommandName = "Map" Then
            Response.Redirect("Basemap.aspx?zoom=yes")
        End If
    End Sub

    Private Sub btnAgeZoomto_Click(sender As Object, e As EventArgs) Handles btnAgeZoomto.Click
        Response.Redirect("Basemap.aspx")
    End Sub

    Private Sub btnZoomTo_Click(sender As Object, e As EventArgs) Handles btnZoomTo.Click
        Response.Redirect("Basemap.aspx")
    End Sub

    Sub generateReport(type As String, erf As String, page As String)
        Dim query As String
        myConn.Open()
        If page = "address" Or page = "erf" Then
            query = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where B.PARCEL_LAB LIKE '" & erf & "'"
        Else
            query = "select * from potch_sde.sde.ERFINLIGTING a inner join potch_sde.sde.LANDPARCEL b on a.Erfverwysi = b.LIS_KEY where a.Straatadre = '" & Session("arrearsAddress") & "' and a.Eienaarnaa = '" & Session("owner") & "'"
        End If
        cmd = New SqlCommand(query, myConn)
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Do While rdr.Read
                Session("owner") = rdr.Item("Eienaarnaa")
                Session("address") = rdr.Item("Straatadre")
                Session("zone") = rdr.Item("Soneringsk")
                Session("wyk") = rdr.Item("Ward")
                Session("size") = rdr.Item("Grootte")
                Session("grond") = rdr.Item("Verbeterde")
                Session("omskryf") = rdr.Item("Erfbeskryw")
                Session("erfInfo") = rdr.Item("Erfverwysi")
                Session("erfNo") = rdr.Item("PARCEL_LAB")
                Exit Do
            Loop
        End If
        myConn.Close()
        Response.Redirect("ErfReport.aspx")
    End Sub

    Protected Sub ExportToExcel(query As String, tableName As String)

        Dim strConnString As String = ConfigurationManager.ConnectionStrings("Potch_sde").ConnectionString
        Dim dt As DataTable
        Using con As New SqlConnection(strConnString)
            Using cmd As New SqlCommand(query)
                Using sda As New SqlDataAdapter()
                    cmd.Connection = con
                    sda.SelectCommand = cmd
                    'Using dt As New DataTable()
                    dt = New DataTable
                    sda.Fill(dt)
                    dt.TableName = tableName
                    'End Using
                End Using
            End Using
        End Using

        Dim wb As New Excel.XLWorkbook
        wb.Worksheets.Add(dt)
        ' Prepare the response
        Dim httpResponse As HttpResponse = Response
        httpResponse.Clear()
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        httpResponse.AddHeader("content-disposition", "attachment;filename=" & tableName & ".xlsx")

        ' Flush the workbook to the Response.OutputStream
        Using memoryStream As New MemoryStream()
            wb.SaveAs(memoryStream)
            memoryStream.WriteTo(httpResponse.OutputStream)
            memoryStream.Close()
        End Using

        httpResponse.[End]()

    End Sub
  

   
End Class